/** Reporter catch error **/
(function(g,f){typeof exports==='object'&&typeof module!=='undefined'?module.exports=f():typeof define==='function'&&define.amd?define(f):(g=g||self,g.ErrorReporter=f());}(this,function(){'use strict';function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}function assign(target) {
  if (target == null) {
    // TypeError if undefined or null
    throw new TypeError('Cannot convert undefined or null to object');
  }

  var to = Object(target);

  for (var index = 0; index < (arguments.length <= 1 ? 0 : arguments.length - 1); index++) {
    var nextSource = index + 1 < 1 || arguments.length <= index + 1 ? undefined : arguments[index + 1];

    if (nextSource != null) {
      // Skip over if undefined or null
      for (var nextKey in nextSource) {
        // Avoid bugs when hasOwnProperty is shadowed
        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
          to[nextKey] = nextSource[nextKey];
        }
      }
    }
  }

  return to;
}

var ErrorReporter =
/*#__PURE__*/
function () {
  function ErrorReporter(option) {
    var _this = this;

    _classCallCheck(this, ErrorReporter);

    this.report = function (data) {
      var isAlert = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      // 只有手机环境才会弹出错误
      if (isAlert && !window.navigator.platform.match(/mac|win/i)) {
        alert(JSON.stringify(data, null, 4));
      }

      var reportData = assign({}, data, {
        UA: window.navigator.userAgent,
        platform: window.navigator.platform,
        screen: [window.screen.availWidth, window.screen.availHeight].join(','),
        url: window.location.href,
        localTime: Date.now(),
        project: _this.option.project
      });
      reportData = _this.option.onBeforeSend(reportData);

      if (reportData) {
        var content = encodeURIComponent(JSON.stringify(reportData));
        new Image().src = "".concat(_this.option.api, "?t=").concat(Date.now(), "&content=").concat(content);
      }
    };

    if (!option.project) {
      console.error('project不能为空');
      return;
    }

    this.option = assign({
      api: 'https://api.pingfang.com/wechat/v1.0/remind/error-by-front',
      onBeforeSend: function onBeforeSend(data) {
        return data;
      }
    }, option);

    this._addEvent();
  }

  _createClass(ErrorReporter, [{
    key: "_addEvent",
    value: function _addEvent() {
      var _this2 = this;

      var errorConfig = assign({
        log: true,
        alert: true,
        limit: 2
      }, this.option.error);
      var reportNum = 1;

      var handleError = function handleError(JSRuntimeError) {
        if (reportNum > errorConfig.limit) {
          return;
        }

        reportNum += 1;

        if (errorConfig.log && JSRuntimeError.stack) {
          console.log('错误栈信息', JSRuntimeError.stack);
        }

        _this2.report({
          JSRuntimeError: JSRuntimeError
        }, errorConfig.alert);
      }; // 获取详细的错误信息


      if (window.addEventListener) {
        window.addEventListener('error', function (messageOrEvent) {
          var _ref = messageOrEvent || {},
              _ref$message = _ref.message,
              message = _ref$message === void 0 ? '' : _ref$message,
              _ref$filename = _ref.filename,
              filename = _ref$filename === void 0 ? '' : _ref$filename,
              _ref$lineno = _ref.lineno,
              lineno = _ref$lineno === void 0 ? 0 : _ref$lineno,
              _ref$colno = _ref.colno,
              colno = _ref$colno === void 0 ? 0 : _ref$colno,
              _ref$error = _ref.error,
              error = _ref$error === void 0 ? {} : _ref$error;

          handleError({
            message: message,
            filename: filename,
            stack: error && error.stack,
            lineno: lineno,
            colno: colno,
            error: error
          });
        }); // 捕获Promise错误

        window.addEventListener('unhandledrejection', function (event) {
          event.promise && event.promise["catch"](function (res) {
            res && handleError({
              message: res.message,
              promiseError: 1,
              reason: event.reason,
              stack: res && res.stack
            });
          });
        });
      } else {
        window.onerror = function (event, source, lineno, colno, error) {
          handleError({
            message: event,
            filename: source,
            stack: error && error.stack,
            lineno: lineno,
            colno: colno,
            error: error
          });
        };
      }
    }
  }]);

  return ErrorReporter;
}();return ErrorReporter;}));