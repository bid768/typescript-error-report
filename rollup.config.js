import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

export default {
  input: 'lib/index.js',
  output: {
    file: './dist/index.js',
    format: 'umd',
    name: 'ErrorReporter',
    banner: `/** ErrorReporter catch error **/`,
    sourcemap: false,
    compact: true,
  },
  watch: {
    include: 'src/**',
  },
  plugins: [
    // rollupTypescript(),
    resolve(),
    babel({
      exclude: 'node_modules/**' // only transpile our source code
    })
  ]
};
