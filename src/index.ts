interface Option {
    // 上报日志接口
    api: string;
    project: string;
    error: {
        // 是否log错误信息
        log: boolean,
        // 是否在非pc环境下alert错误信息
        alert: boolean,
        // 错误上报限制，避免死循环
        limit: 2,
    },
    // 是否上报
    onBeforeSend(data: any): any;
}

function assign(target, ...sources) {
    if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      let to = Object(target);

      for (var index = 0; index < sources.length; index++) {
        var nextSource = sources[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (let nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
}

export default class ErrorReporter {
    option: Option
    constructor(option: Option) {
        if (!option.project) {
            console.error('project不能为空');
            return;
        }

        this.option = assign({
            api: 'https://api.pingfang.com/wechat/v1.0/remind/error-by-front',
            onBeforeSend: (data) => data,
        }, option);

        this._addEvent();
    }

    _addEvent() {
        const errorConfig = assign({
            log: true,
            alert: true,
            limit: 2,
        }, this.option.error);

        let reportNum = 1;
        const handleError = (JSRuntimeError: any) => {
            if (reportNum > errorConfig.limit) {
                return;
            }
            reportNum += 1;

            if (errorConfig.log && JSRuntimeError.stack) {
                console.log('错误栈信息', JSRuntimeError.stack);
            }

            this.report({
                JSRuntimeError,
            }, errorConfig.alert);
        }

        // 获取详细的错误信息
        if (window.addEventListener) {
            window.addEventListener('error', (messageOrEvent) => {
                const { message = '', filename = '', lineno = 0, colno = 0, error = {} } = messageOrEvent || {};
                handleError({
                    message,
                    filename,
                    stack: error && error.stack,
                    lineno,
                    colno,
                    error,
                });
            });

            // 捕获Promise错误
            window.addEventListener('unhandledrejection', function (event) {
                event.promise && event.promise.catch(function (res) {
                    res && handleError({
                        message: res.message,
                        promiseError: 1,
                        reason: event.reason,
                        stack: res && res.stack,
                    });
                });
            });
        } else {
            window.onerror = (event, source, lineno, colno, error) => {
                handleError({
                    message: event,
                    filename: source,
                    stack: error && error.stack,
                    lineno,
                    colno,
                    error,
                });
            }
        }
    }

    report = (data: any, isAlert: boolean = false) => {
        // 只有手机环境才会弹出错误
        if (isAlert && !window.navigator.platform.match(/mac|win/i)) {
            alert(JSON.stringify(data, null, 4));
        }

        let reportData = assign({}, data, {
            UA: window.navigator.userAgent,
            platform: window.navigator.platform,
            screen: [window.screen.availWidth, window.screen.availHeight].join(','),
            url: window.location.href,
            localTime: Date.now(),
            project: this.option.project,
        });

        reportData = this.option.onBeforeSend(reportData)
        if (reportData) {
            const content = encodeURIComponent(JSON.stringify(reportData));
            new Image().src = `${this.option.api}?t=${Date.now()}&content=${content}`;
        }
    }
}
